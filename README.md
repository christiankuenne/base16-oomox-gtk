# base16-oomox-gtk

This repository is meant to work with [chriskempson/base16](https://github.com/chriskempson/base16).
It provides a simple template that can be used with the base16 color schemes to generate a functional config file for 
[themix-project/oomox-gtk-theme](https://github.com/themix-project/oomox-gtk-theme).